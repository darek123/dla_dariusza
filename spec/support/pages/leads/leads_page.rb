class LeadsPage < AbstractPage
  include PageObject
  include LeadModal
  include RelatedToPicker

  page_url "https://app.futuresimple.com/leads"

  a(:add_new_lead, :id => "leads-new")
  ul(:notes_list, :class => "feed-items")
  
 def get_lead content
    note = []
    notes_list_element.when_visible.list_item_elements.each do |item|
	#puts item.text
	note << item if item.text.include? content
	end
    note
  
end
end