module LeadModal
  include PageObject

  text_field(:first_name_content, :name => "first_name", :placeholder => "First Name")
  text_field(:last_name_content, :name => "last_name", :placeholder => "Last Name*")
  text_area(:add_note_content, :placeholder => "Add a note about this lead.") 
  
  button(:save_lead, :text => "Save")
  link(:update_task, :text => "Update")
  link(:cancel_task, :text => "Cancel")
  link(:delete_task, :text => "Delete Task")

end
