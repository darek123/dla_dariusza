require 'spec_helper'

def get_created_lead_from_list content
  on(LeadsPage).get_lead content
end

shared_examples "Create lead and check it" do |content, remove_date, related, owner|
  it "Open add lead form" do
    p related_name if related_name
    on(LeadsPage).add_new_lead_element.when_visible.click
  end

  it "Enter lead content first name" do
    on(LeadsPage).first_name_content_element.when_visible(10).value = "user"
  end
  
  it "Enter lead content last name" do
    on(LeadsPage).last_name_content_element.when_visible(10).value = "user2"
  end
  
  it "Save lead" do
    on(LeadsPage).save_lead
  end
  
   it "Enter lead content add note" do
    on(LeadsPage).add_note_content_element.when_visible(10).value = content
  end

  it "Save note" do
    on(LeadsPage).save_lead_element.when_visible(10).click
  end
  
   it "Check if note is visible" do
    note = get_created_lead_from_list content
    expect(note[0]).not_to be_nil
  end
   
end

describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end
  
  describe "Add floating task" do
    include_examples "Create lead and check it", Faker::Lorem.sentence, true, false do
      let(:related_name) { nil }
      let(:owner) { false }
    end
  end
end
